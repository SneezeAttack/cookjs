# CookJS

A Phaser 3 project for practice and fun. Uses [Babel 7](https://babeljs.io) and [Webpack 4](https://webpack.js.org/)


## Live Demo
[Demo Site](http://cookjs.duckdns.org:8070)
